const express = require("express");
const router = express.Router();

const { getAllMovies, searchMovie } = require("../controllers/movieController");
//all movies route, its slow
router.route("/movies").get(getAllMovies);

//search movies with title containing queryTitle : searchMovie?queryTitle=Tita
router.route("/searchMovie").get(searchMovie);

module.exports = router;
