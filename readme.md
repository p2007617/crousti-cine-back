## config

Ajouter un .env a la racine du projet contenant:

```.env
MONGO_MFLIX_DATABASE_URL="mongodb+srv://basic_user:Wce10kGlrfSVyo3i@cluster0.3cubela.mongodb.net/sample_mflix"
JWT_SECRET="SECRETKEY"
```

`SECRETKEY` est remplacable par n'importe quelle chaine de caractère.

Installer les dépendances avec la commande `npm i`.
Lancer le serveur en local avec la commande `npm run dev`.
Celui-ci est en écoute sur le port 3000.

### Routes

#### Movies

Les routes de films ne necessitent pas de connection utilisateur.

- GET /Movies : envoie un tableau de tous les films de la base de donnée, attention c'est assez long il y bcp de films.
- GET /searchMovie?queryTitle=TITLE : envoie un tableau des films dont le titre contient TITLE.

#### Users

Routes user ne necessitant pas de token de connexion :

- POST /register : ajoute l'utilisateur en base s'il n'existe pas déjà.
- POST /login : si le nom d'utilisateur et le mdp sont correct, renvoie un token de connection

Routes necessitant un token de connexion Bearer Token en header :

- POST /addFavMovie : ajoute un film a la liste de favoris de l'utilisateur connecté.
- POST /removeFavMovie : retire un film de la liste des favoris.
- GET /getFavMovies : renvoie la liste des films favoris de l'utilisateur connecté.

#### Parametres et corps de requêtes

Pour connaitre les parametres necessaire lors des requêtes, voir openapi.html
