const request = require("supertest");
const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server");
const MovieModel = require("../../models/MovieModel");
const { DB_URL } = require("../../db");
const app = require("../../../app");

let mongoServer;

beforeAll(async () => {
  mongoServer = await MongoMemoryServer.create({
    binary: {
      version: "6.0.4",
    },
  });
  const mongoUri = mongoServer.getUri();
  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  await MovieModel.create([{ title: "Inception" }, { title: "The Matrix" }]);
}, 3600000);
afterAll(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe("Movie Routes", () => {
  it("should get all movies", async () => {
    const res = await request(app).get("/movies");

    expect(res.status).toBe(200);
    expect(res.body.movies).toBeInstanceOf(Array);
    expect(res.body.movies.length).toBe(2);
  });

  it("should search for movies which title contains queryTitle", async () => {
    const res = await request(app).get("/searchMovie?queryTitle=Incept");

    expect(res.status).toBe(200);
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body[0]["title"]).toBe("Inception");
  });
});
