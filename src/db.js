const mongoose = require("mongoose");
require("dotenv").config();

const DB_URL = process.env.MONGO_MFLIX_DATABASE_URL;

async function connectDB() {
  await mongoose.connect(DB_URL, {});
}

const mongoConnection = mongoose.connection;
module.exports = { connectDB, mongoConnection, DB_URL };
