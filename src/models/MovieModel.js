const Mongoose = require("mongoose");
const MovieSchema = new Mongoose.Schema(
  {
    title: { type: String },
    plot: { type: String },
    year: { type: Number },
    genres: { type: [String] },
    runtime: { type: Number },
    directors: { type: [String] },
    poster: { type: String },
  },
  { collection: "movies" }
);

const MovieModel = Mongoose.model("movie", MovieSchema);
module.exports = MovieModel;
