const UserModel = require("../models/UserModel");
const jwt = require("jsonwebtoken");
const { extractBearerToken } = require("../middleware/jwtMiddleware");

exports.login = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password) {
    return res.status(400).json({ error: "bad json" });
  }

  const user = await UserModel.findOne({ username, password });

  if (!user) {
    return res.status(400).json({ error: "no such user" });
  }

  const token = jwt.sign(
    {
      id: user.id,
      username: user.username,
    },
    process.env.JWT_SECRET,
    { expiresIn: "3 hours" }
  );
  return res.json({ token: token });
};

exports.register = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  let users;
  if (!username || !password) {
    return res.status(400).json({ error: "bad json" });
  }
  try {
    users = await UserModel.findOne({ username });
  } catch {
    return res.status(400).json({ error: "error while checking users" });
  }
  if (users) {
    return res.status(400).json({ error: "user already exists" });
  }
  try {
    const newUser = new UserModel({ username, password });
    await newUser.save();
  } catch (err) {
    return res
      .status(400)
      .json({ message: "error while creating user", error: err.message });
  }
  return res.status(200).json({ message: "user created succesfully" });
};

exports.addFavMovie = async (req, res, next) => {
  try {
    const userId = req.user.id; // Supposons que l'ID de l'utilisateur est stocké dans req.user après validation du token
    const movieId = req.body.movieId; // Supposons que l'ID du film à ajouter est envoyé dans le corps de la requête

    // Vérifier si l'utilisateur existe
    const user = await UserModel.findById(userId);

    if (!user) {
      return res.status(404).json({ error: "user not found" });
    }

    // Vérifier si le film est déjà dans la liste des favoris
    if (user.favMovies.includes(movieId)) {
      return res.status(400).json({ error: "Movie already faved" });
    }

    // Ajouter le film à la liste des favoris
    user.favMovies.push(movieId);

    // Sauvegarder les modifications
    await user.save();

    return res.status(200).json({ message: "Movie faved" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal error" });
  }
};

exports.removeFavMovie = async (req, res, next) => {
  try {
    const userId = req.user.id; // Supposons que l'ID de l'utilisateur est stocké dans req.user après validation du token
    const movieId = req.body.movieId; // Supposons que l'ID du film à retirer est envoyé dans le corps de la requête

    // Vérifier si l'utilisateur existe
    const user = await UserModel.findById(userId);

    if (!user) {
      return res.status(404).json({ error: "user not found" });
    }

    // Vérifier si le film est dans la liste des favoris
    if (!user.favMovies.includes(movieId)) {
      return res.status(400).json({ error: "movie not faved" });
    }

    // Retirer le film de la liste des favoris
    user.favMovies = user.favMovies.filter((id) => id.toString() !== movieId);

    // Sauvegarder les modifications
    await user.save();

    return res.status(200).json({ message: "movie unfaved" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "internal error" });
  }
};

exports.getFavMovies = async (req, res, next) => {
  try {
    const userId = req.user.id; // Supposons que l'ID de l'utilisateur est stocké dans req.user après validation du token

    // Vérifier si l'utilisateur existe
    const user = await UserModel.findById(userId).populate({
      path: "favMovies",
      model: "movie",
      select: {
        title: 1,
        plot: 1,
        year: 1,
        genres: 1,
        runtime: 1,
        directors: 1,
        poster: 1,
      },
    });

    if (!user) {
      return res.status(404).json({ error: "user not found" });
    }

    // Extraire les informations sur les films favoris
    const favMovies = user.favMovies;

    return res.status(200).json({ favMovies });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Erreur interne du serveur." });
  }
};
