const request = require("supertest");
const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server");
const UserModel = require("../../models/UserModel");
const MovieModel = require("../../models/MovieModel");
const { DB_URL } = require("../../db");
const app = require("../../../app");

let mongoServer;

beforeAll(async () => {
  mongoServer = await MongoMemoryServer.create({
    binary: {
      version: "6.0.4",
    },
  });
  const mongoUri = mongoServer.getUri();
  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  await UserModel.create([
    { username: "Alice", password: "hello123", favMovies: [] },
    {
      username: "Bob",
      password: "azerty123",
      favMovies: [new mongoose.Types.ObjectId(1)],
    },
    ,
    {
      username: "Charles",
      password: "abcdefgh",
      favMovies: [
        new mongoose.Types.ObjectId(1),
        new mongoose.Types.ObjectId(2),
      ],
    },
  ]);
  await MovieModel.create({
    _id: new mongoose.Types.ObjectId(1),
    title: "movie 1",
  });
}, 3600000);
afterAll(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe("POST /login", () => {
  it("should return a token when valid username and password are provided", async () => {
    const response = await request(app)
      .post("/login")
      .send({ username: "Alice", password: "hello123" })
      .expect(200);

    expect(response.body).toHaveProperty("token");
  }, 10000);

  it("should return an error when invalid username or password are provided", async () => {
    const response = await request(app)
      .post("/login")
      .send({ username: "invaliduser", password: "invalidpassword" })
      .expect(400);

    expect(response.body).toHaveProperty("error");
  }, 10000);
});

describe("POST /register", () => {
  it("should create a new user", async () => {
    // Effectuer une requête POST /register avec de nouvelles informations utilisateur
    const response = await request(app)
      .post("/register")
      .send({ username: "newuser", password: "newpassword" })
      .expect(200);

    // Vérifiez si l'utilisateur a été ajouté à la base de données
    const user = await UserModel.findOne({ username: "newuser" });
    expect(user).toBeDefined();
  }, 10000);

  it("should return an error when trying to register with an existing username", async () => {
    const response = await request(app)
      .post("/register")
      .send({ username: "Alice", password: "hello123" })
      .expect(400);

    // Assurez-vous que la réponse contient une erreur
    expect(response.body).toHaveProperty("error");
  }, 10000);

  // Vous pouvez ajouter d'autres tests pour valider d'autres scénarios, par exemple, un mot de passe manquant, etc.
});

describe("POST /addFavMovie", () => {
  it("should add a favMovie to a logged user", async () => {
    //log user
    const tokenRequest = await request(app)
      .post("/login")
      .send({ username: "Alice", password: "hello123" })
      .expect(200);

    user = await UserModel.findOne({ username: "Alice" });
    expect(user.favMovies.length).toBe(0);

    const response = await request(app)
      .post("/addFavMovie")
      .set("Authorization", `Bearer ${tokenRequest.body.token}`)
      .send({ movieId: new mongoose.Types.ObjectId(1) })
      .expect(200);

    user = await UserModel.findOne({ username: "Alice" });
    expect(user.favMovies.length).toBe(1);
  }, 10000);

  it("should respond an error when movie is already added", async () => {
    //log user
    const tokenRequest = await request(app)
      .post("/login")
      .send({ username: "Charles", password: "abcdefgh" })
      .expect(200);

    user = await UserModel.findOne({ username: "Charles" });
    expect(user.favMovies.length).toBe(2);

    const response = await request(app)
      .post("/addFavMovie")
      .set("Authorization", `Bearer ${tokenRequest.body.token}`)
      .send({ movieId: user.favMovies[0] })
      .expect(400);

    user = await UserModel.findOne({ username: "Charles" });
    expect(user.favMovies.length).toBe(2);
  }, 10000);
});

describe("POST /removeFavMovie", () => {
  it("should remove a favMovie from a logged user", async () => {
    //log user
    const tokenRequest = await request(app)
      .post("/login")
      .send({ username: "Bob", password: "azerty123" })
      .expect(200);

    //find user info
    user = await UserModel.findOne({ username: "Bob" });
    expect(user.favMovies.length).toBe(1);

    const response = await request(app)
      .post("/removeFavMovie")
      .set("Authorization", `Bearer ${tokenRequest.body.token}`)
      .send({ movieId: user.favMovies[0] })
      .expect(200);

    user = await UserModel.findOne({ username: "Bob" });
    expect(user.favMovies.length).toBe(0);
  }, 10000);
});

describe("GET /getFavMovie", () => {
  it("should get favMovies of a logged user", async () => {
    //log user
    const tokenRequest = await request(app)
      .post("/login")
      .send({ username: "Charles", password: "abcdefgh" })
      .expect(200);

    const response = await request(app)
      .get("/getFavMovies")
      .set("Authorization", `Bearer ${tokenRequest.body.token}`)
      .send()
      .expect(200);

    user = await UserModel.findOne({ username: "Charles" });
    console.log(user);
    expect(user.favMovies).toBeInstanceOf(Array);
    expect(user.favMovies.length).toBe(2);
  }, 10000);
});
