const MovieModel = require("../models/MovieModel");

exports.getAllMovies = async (req, res, next) => {
  try {
    const movies = await MovieModel.find().lean().limit(100).exec();
    return res.status(200).json({ movies });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Cannot get movies." });
  }
};

exports.searchMovie = async (req, res, next) => {
  const { queryTitle } = req.query;

  try {
    const movies = await MovieModel.find({
      title: { $regex: queryTitle, $options: "i" },
    })
      .lean()
      .exec();
    return res.status(200).json(movies);
  } catch {
    return res.status(400).json({ error: "Cannot search for movie" });
  }
};
