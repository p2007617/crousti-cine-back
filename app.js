const express = require("express");

const app = express(); // Utilisation de bodyParser pour parser le corps des requêtes
app.use(express.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
// Utilisation des routes définies dans moviesRoutes
app.use(
  "/",
  require("./src/routes/moviesRoutes"),
  require("./src/routes/userRoutes")
);

module.exports = app;
