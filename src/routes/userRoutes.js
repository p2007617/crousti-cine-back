const express = require("express");
const router = express.Router();
const { validateToken } = require("../middleware/jwtMiddleware");

const {
  login,
  register,
  addFavMovie,
  removeFavMovie,
  getFavMovies,
} = require("../controllers/userController");

router.route("/login").post(login);
router.route("/register").post(register);

router.route("/addFavMovie").post(validateToken, addFavMovie);
router.route("/removeFavMovie").post(validateToken, removeFavMovie);
router.route("/getFavMovies").get(validateToken, getFavMovies);
module.exports = router;
