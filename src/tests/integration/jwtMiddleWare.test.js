const request = require("supertest");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server");
const UserModel = require("../../models/UserModel");
const MovieModel = require("../../models/MovieModel");
const { DB_URL } = require("../../db");
const app = require("../../../app");

let mongoServer;

beforeAll(async () => {
  mongoServer = await MongoMemoryServer.create({
    binary: {
      version: "6.0.4",
    },
  });
  const mongoUri = mongoServer.getUri();
  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  await UserModel.create([
    { username: "Alice", password: "hello123", favMovies: [] },
    {
      username: "Bob",
      password: "azerty123",
      favMovies: [new mongoose.Types.ObjectId(1)],
    },
    ,
    {
      username: "Charles",
      password: "abcdefgh",
      favMovies: [new mongoose.Types.ObjectId(1)],
    },
  ]);
  await MovieModel.create({
    _id: new mongoose.Types.ObjectId(1),
    title: "movie 1",
  });
}, 3600000);
afterAll(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe("Middleware: validateToken", () => {
  it("should pass with a valid token", async () => {
    // log user
    const tokenRequest = await request(app)
      .post("/login")
      .send({ username: "Alice", password: "hello123" })
      .expect(200);

    req = await request(app)
      .get("/getFavMovies")
      .set("Authorization", `Bearer ${tokenRequest.body.token}`)
      .expect(200);
  });

  it("should return status 401 if token is wrong", async () => {
    // Envoyer une requête avec un token invalide
    await request(app)
      .get("/getFavMovies")
      .set("Authorization", "Bearer blablbalbalba")
      .expect(401);
  });

  it("should return 401 if no token provided", async () => {
    // Envoyer une requête sans token
    await request(app).get("/getFavMovies").expect(401);
  });
});
