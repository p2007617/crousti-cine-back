require("dotenv").config();
const app = require("./app");
const mongoose = require("mongoose");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const { connectDB, mongoConnection } = require("./src/db");

const PORT = 3000;

// Connexion à la base de données
connectDB();
mongoConnection.on("error", () => {
  console.log(console, "Cannot connect to db.");
});
mongoConnection.once("open", () => {
  console.log("Connected to crousti cine db.");
});

const server = app.listen(PORT, () => {
  console.log(`Server connected to port ${PORT}.`);
});

process.on("unhandledRejection", (err) => {
  console.log("Error", err);
  server.close(() => process.exit(1));
});
