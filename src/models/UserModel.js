const Mongoose = require("mongoose");
const UserSchema = new Mongoose.Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      minlength: 6,
      required: true,
    },
    favMovies: {
      type: [{ type: Mongoose.Schema.Types.ObjectId, ref: "Movies" }],
    },
  },
  { collection: "users" }
);

const UserModel = Mongoose.model("user", UserSchema);
module.exports = UserModel;
