const jwt = require("jsonwebtoken");

// Middleware pour valider un token
exports.validateToken = (req, res, next) => {
  // Récupérer le token de l'en-tête Authorization
  const tokenHeader = req.header("Authorization");

  // Vérifier s'il y a un token
  if (!tokenHeader) {
    return res
      .status(401)
      .json({ msg: "Accès non autorisé. Aucun token fourni." });
  }

  // Extraire le token du format "Bearer <token>"
  const token = tokenHeader.split(" ")[1];

  try {
    // Vérifier le token
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    // Ajouter les informations décodées à la demande
    req.user = {
      id: decoded.id,
      username: decoded.username,
    };

    // Passer à l'étape suivante du middleware
    next();
  } catch (err) {
    // Gérer les erreurs liées au token
    res.status(401).json({ msg: "Token non valide." });
  }
};
